/**
 * send mail through amazon testcase is available at  ApplaneDB/test/MailSendingService.js  TestCase Name = "sending mail through Amazon"
 *
 * AMAZON docs is available at http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SES.html#sendRawEmail-property
 */
var Q = require("q");
var ejs = require("ejs");



exports.sendMail = function (options ,callback) {
    console.log("options are",options);
    if (options.data) {
        if (options.html) {
            options.html = ejs.render(options.html, options.data);
        } else {
            options.html = ejs.render(options.text, options.data);
        }
    }
    getMailCredentials(options, function(err , result) {
        if(err) {
            console.log("error is ",err)
        }
        else {
        if (result.type === "nodemailer") {
            console.log("return credentials ",result)
            sendMailFromNodeMailer(result, options).then(function(data){
                console.log("data is");
            });          
                
        }
    }
    })
};





function sendMailFromNodeMailer(credentials, options ) {
    var D = Q.defer();
    var smtpTransport = require("nodemailer").createTransport(credentials);
    // if (options && options.async) {
    //     D.resolve();//we will resovle immediately
    // }
    smtpTransport.sendMail(options, function (err) {
        if (options && options.mailLogs) {
            updateMailStatus(options, err);
        }
        if (!options || !options.async) {
            if (err) {
                D.reject(err);
            } else {
                D.resolve();
            }
        }
    });
    return D.promise;
}



function getMailCredentials(options ,callback) {
    
    var mailcredentials = require("../config.js").mailcredentials;
    // var d = Q.defer();
    if (mailcredentials.nodemailer.USERNAME) {
        username = mailcredentials.nodemailer.USERNAME;
        password = mailcredentials.nodemailer.PASSWORD;
        if (!password) {
           callback(new Error("Provide password for using nodemailer send mail service"),null);
            
        }
        console.log("\n oooo",{auth: {user: username, pass: password}, type: "nodemailer", service: "Gmail"})
        callback(null,{auth: {user: username, pass: password}, type: "nodemailer", service: "Gmail"});
        
    }
}
    
    

