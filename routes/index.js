var express = require('express');
var router = express.Router();
var api=require('./data.js').api;
var nodemailer = require("nodemailer");
var fs=require('fs-extra');
var path =require('path');
var mkdirp = require('mkdirp');
var smtpTransport = nodemailer.createTransport();
var Q = require('q');



/*
  * @name GET /home
  * @desc
  * To go to the home page
*/

router.get('/home', function(req, res, next) {
  api.home(); 
  res.send('this is home page');
});

/*
  * @name PUT /user
  * @desc
  * To update the information of user
*/
router.put('/user/:email', function(req,res,next) {
  console.log("new updation is :" , req.body);
  api.updateUser({"email":req.params.email},req.body,function(err,result){
    if(err) {
      console.log("error is :", err);
      res.status(400).send(err);
    }
    else {
      console.log("syccessful");
      api.getUser({"email": req.params.email},function(err,result){
      if(err) { console.log("error is :",err);  }
      else { console.log("succesful",result);res.send(result); }
      });

    }
  });
});

/*
  * @name POST /userS
  * @desc
  * To get all the users from database
*/
router.post('/users', function(req,res,next) {
  console.log("getting all users ..",req.body);
  api.getUsers(req.body.member,function(err, result) {
   if(err) {
     console.log("error in getting all users is :",err);
    } else {
     console.log("the hwljfgdk:", result);
     res.send(result);
   }
  }
  );
});

/*
  * @name PUT /reset
  * @desc
  * To reset the password of user
*/
router.put('/reset/:email',function(req,res,next) {
  console.log("email is :" ,req.params.email);
  console.log("updation is :" ,req.body);
  api.updateUser({"email":req.params.email},req.body,function(err,result){
    if(err) {
      console.log("error is :", err);
      res.status(400).send(err);
    } else {
        console.log("succesful password updation :", result);
        api.login({"email" :req.params.email , "pass" :req.body.pass} ,function(err,resu) {
        if(err) {
          console.log("error with new pass with :" ,err);
          res.send(err);
        } else {
          console.log("now the user is : ",resu[0].email);
          smtpTransport.sendMail({
          from: "teamtaskhandling@gmail.com", // sender address
          to: resu[0].email, // comma separated list of receivers
          subject: "Password Reset", // Subject line
          text: "New password for your account is given below. \n\n"+resu[0].pass// plaintext body
            },function(error, response){
                    if(error){
                     console.log(error);
                    } else {
                      console.log("Message sent: " + response);
                      res.send(response);
                    }
              });
          
          }
        });
      }
  });
});

/*
  * @name POST /login
  * @desc
  * To validate login 
*/



router.post('/login', function(req, res, next) {
  console.log('index.' + JSON.stringify(req.body));

  api.login(req.body).then(function(value){
      console.log(".....oops",value);
      res.send(value);         
    }
  ); 
});

/*
  * @name GET/user
  * @desc
  * To verify user account
*/

router.get('/user/:verId/:emailID',function(req,res,next){
  console.log("what i got is :" + req.params.verId + "and    " + req.params.emailID);
  api.getUser({"email": req.params.emailID, "vcode" : req.params.verId},function(err,result){
     if(err) { console.log("error is :",err);}
     else {console.log("succesful",result);res.send(result);}
  });
});

/*
  * @name GET/member
  * @desc
  * To get the details of project member individually
*/

router.get('/member/:emailID',function(req,res,next){
  console.log("what i got is :" + req.params.emailID);
  api.getUser({"email": req.params.emailID},function(err,result){
     if(err) { console.log("error is :",err);}
     else {console.log("succesful",result);res.send(result);}
  });
});

/*
  * @name POST/register
  * @desc
  * To register a new user with his details
*/

router.post('/register', function(req, res, next) {
  res.setHeader("Content-Type","application/json");
  console.log("there is register");
  api.register(req.body,function(err,result){
    if (err) { 
      console.log("there is error in insertion",err);
      err = {"name" : err};
      res.status(400).send(err);
    } else {
        console.log('result register:' + JSON.stringify(result));
        smtpTransport.sendMail({
        from: "teamtaskhandling@gmail.com", // sender address
        to: result.email, //you can put here (result.email) to send the email to user  comma separated list of receivers
        subject: "Registration Successful", // Subject line
        html: "Congratulations! You have successfully registere your account for Task Manager.<br> Your EMail is :" + result.email + "<br>Your Password is: "+ result.pass +"<br>Click this link to verify your email-id <br > http://192.168.100.167:4000/#/verify/" + result.verificationID + '/email/' + result.email +"><b>Verify your Email.</b>"
        },  function(error, response){
              if(error){
                console.log(error);
                } else {
                    console.log("Message sent: " + response.message);
                    if(!req.body.projId==""){
                    api.updateProject1({"name" : req.body.projId ,"userId":req.body.ownerId},{memberEmail:result.email},function(err,result){
                      if(err){ 
                        console.log("error in updation is :" , err);
                        res.status(400).send(err);
                        } else { 
                            console.log('updated project' +JSON.stringify(result));
                            api.getProject({"userId" : req.body.ownerId},function(err, result) {
                              if (err) { console.log("errorrrr....");
                              res.status(400).send(err);
                              } else {
                                  console.log('all the tasks..............');
                                  res.send(result);
                              }
    
                            });
                        }
                    });
                    } else {
                        res.send(result);
                    }
                  }
            });
            
      }
    
  }); 
});

/*
  * @name GET/project
  * @desc
  * To get aal the projects of a user
*/

router.get('/project/:userID', function(req, res, next) {
  res.setHeader("Content-Type","application/json");
  api.getProject({"userId" : req.params.userID},function(err, result) {
    if (err) {
      if(err[0].code == 11000) {
          res.status(400).send(err);console.log("errorrrr....")
      } } else {
        console.log('all the projects..............')
        res.send(result);
      }
    
  }); 
   
});

/*
  * @name POST /project
  * @desc
  * To create a new project for the user
*/

router.post('/project',function(req,res,next) {
     console.log("/project :" +JSON.stringify(req.body));
     api.addProject(req.body,function(err,result){
      if(err){
        res.status(400).send(err);
      } else {
        console.log('add project' +JSON.stringify(result));
        res.send(result);
      }
     });
});

/*
  * @name PUT /project
  * @desc
  * To update the details of project
*/

router.put('/project/:pName/:userid',function(req,res,next) {
 console.log("/project :" +JSON.stringify(req.body));
 api.updateProject({"name" : req.params.pName ,"userId": req.params.userid},req.body,function(err,result){
  if(err){ 
    console.log("error in updation is :" , err);
    res.status(400).send(err);
  } else { 
     console.log('updated project' +JSON.stringify(result));
     api.getProject({"userId" : req.params.userid},function(err, result) {
       if (err) { console.log("errorrrr....");
         res.status(400).send(err);
       } else {
         console.log('all the taks..............');
         res.send(result);
       }

     });
   }
 });
});

/*
  * @name GET/project/task
  * @desc
  * To get all the tasks of a project
*/

router.get('/project/task/:projID', function(req, res, next) {
 res.setHeader("Content-Type","application/json");
 
 api.getTask({"projId" : req.params.projID},function(err, result) {
  if (err) { console.log("errorrrr....");
        res.status(400).send(err);
  } else {
    console.log('all the taks..............');
    res.send(result);
  }
 }); 
});

/*
  * @name POST /completed
  * @desc
  * To get the owner of the task and send completion mail to owner 
*/

router.post('/completed/:EmailID',function(req,res,next) {
 console.log("completed....",req.body);
 api.getOwner(req.body.userId,function(err,result) {
  if(err) {
   console.log("canttfind user ..");
  }
  else {
   console.log("woow user find",result);
  }
 });
 smtpTransport.sendMail({
   from: "teamtaskhandling@gmail.com", // sender address
   to: req.params.EmailID, //you can put here (result.email) to send the email to user  comma separated list of receivers
   subject: "Task Completion", // Subject line
    // plaintext body
   html: "Your Task Has been completed..the details of task are :.<br>"+req.body.title+"/"+req.body.taskStatus
    }, function(error, response){
       if(error) {
        console.log("error is :",error);
       } else {
        console.log("response is :", response);
       }
    });   

});

/*
  * @name POST /project/task
  * @desc
  * To create new task for any project
*/

router.post('/project/task',function(req,res,next) {
 console.log("/project/task :" +JSON.stringify(req.body));
 api.addTask(req.body,function(err,result){
  if(err){
   res.status(400).send(err);console.log("errorrrr....")
  } else {
    console.log('add task:' +JSON.stringify(result));
    res.send(result);
  }
 });
});

/*
  * @name PUT /project/task
  * @desc
  * To update the information of a task
*/

router.put('/project/task/:taskID/:proj',function(req,res,next) {
 console.log("/project/task :" +JSON.stringify(req.body));
 api.updateTask( {"title": req.params.taskID,"projId" : req.params.proj} ,req.body, function(err,result){
  if(err){
   res.status(404).send(err);
  } else {
    console.log("updtes is ", JSON.stringify(result));
    api.getTask1({"projId" : req.params.proj,"title":req.params.taskID},function(err, result) {
     if (err) { console.log("errorrrr....");
      res.status(400).send(err);
     } else {
       console.log('all the taks..............');
       res.send(result);
     }
    });
  }
 });

});

/*
  * @name POST /image
  * @desc
  * To upload image on the server
*/

router.post('/image',function(req,res,next) {
  var user_Id;
  var sendPath="";
  console.log("thisi s image post .....");
  req.pipe(req.busboy);

  req.busboy.on('file', function (fieldname, file, filename) {
    fieldname = user_Id;
    console.log("fieldname...", fieldname);
    var savePath = path.join('/home/deepak/Deepak Trello/myapp/client/images',fieldname,filename);
    mkdirp.sync(path.dirname(savePath));
    file.pipe(fs.createWriteStream(savePath));
    sendPath = fieldname + "/"+filename;
    console.log("sendpath",sendPath);
    res.send({"picPath":sendPath});
  });

  req.busboy.on('field', function(fieldname, val) {
    user_Id = JSON.parse(val).emailId;
  });

  console.log(req.body);
  console.log(req.file);
  console.log("sendpath",sendPath);
});

/*
  * @name DELETE/project/task
  * @desc
  * To delete the task
*/

router.delete('/project/task',function(req,res,next) {
 console.log("/project/task :" +JSON.stringify(req.body));
 api.deleteTask(req.body,function(err,result){
  if(err){
   res.status(400).send(err);
  } else {
    api.getTask();
  }
 });
});


module.exports = router;


