var express = require('express');
var router = express.Router();
var api=require('./data.js').api;
var nodemailer = require("nodemailer");
var fs=require('fs-extra');
var path =require('path');
var mkdirp = require('mkdirp');
var smtpTransport = nodemailer.createTransport();
var Q = require('q');
/*
  * @name GET/project/task
  * @desc
  * To get all the tasks of a project
*/

router.get('/list/:projname/:email', function(req, res, next) {
 res.setHeader("Content-Type","application/json");
 
 api.getTask({"projName" : req.params.projname , "userEmail" : req.params.email},function(err, result) {
  if (err) { console.log("errorrrr....");
        res.status(400).send(err);
  } else {
    console.log('all the taks..............');
    res.send(result);
  }
 }); 
});

/*
  * @name POST /completed
  * @desc
  * To get the owner of the task and send completion mail to owner 
*/

router.post('/completed/:EmailID',function(req,res,next) {
 console.log("completed....",req.body);
 api.getOwner(req.body.userId,function(err,result) {
  if(err) {
   console.log("canttfind user ..");
  }
  else {
   console.log("woow user find",result);
  }
 });
 smtpTransport.sendMail({
   from: "teamtaskhandling@gmail.com", // sender address
   to: req.params.EmailID, //you can put here (result.email) to send the email to user  comma separated list of receivers
   subject: "Task Completion", // Subject line
    // plaintext body
   html: "Your Task Has been completed..the details of task are :.<br>"+req.body.title+"/"+req.body.taskStatus
    }, function(error, response){
       if(error) {
        console.log("error is :",error);
       } else {
        console.log("response is :", response);
       }
    });   

});

/*
  * @name POST /project/task
  * @desc
  * To create new task for any project
*/

router.post('/add',function(req,res,next) {
 console.log("/project/task :" +JSON.stringify(req.body));
 api.addTask(req.body,function(err,result){
  if(err){
   res.status(400).send(err);console.log("errorrrr....")
  } else {
    console.log('add task:' +JSON.stringify(result));
    res.send(result);
  }
 });
});

/*
  * @name PUT /project/task
  * @desc
  * To update the information of a task
*/

router.put('/update/:taskID/:proj',function(req,res,next) {
 console.log("/project/task :" +JSON.stringify(req.body));
 api.updateTask( {"title": req.params.taskID,"projName" : req.params.proj} ,req.body, function(err,result){
  if(err){
   res.status(404).send(err);
  } else {
    console.log("updtes is ", JSON.stringify(result));
    api.getTask1({"projId" : req.params.proj,"title":req.params.taskID},function(err, result) {
     if (err) { console.log("errorrrr....");
      res.status(400).send(err);
     } else {
       console.log('all the taks..............');
       res.send(result);
     }
    });
  }
 });

});
/*
  * @name DELETE/project/task
  * @desc
  * To delete the task
*/

router.delete('/delete',function(req,res,next) {
 console.log("/project/task :" +JSON.stringify(req.body));
 api.deleteTask(req.body,function(err,result){
  if(err){
   res.status(400).send(err);
  } else {
    api.getTask();
  }
 });
});


module.exports = router;
