var express = require('express');
var router = express.Router();
var api = require('./data.js').api;
var nodemailer = require("nodemailer");
var fs = require('fs-extra');
var path = require('path');
var mkdirp = require('mkdirp');
var smtpTransport = nodemailer.createTransport();
var Q = require('q');
var mailservice = require("../js/MailService.js");

/*
 * @name PUT /user
 * @desc
 * To update the information of user
 */
router.put('/update/:email/:ver_id', function(req, res, next) {
    console.log("new updation is :", req.body);
    console.log("email is ", req.params.email)
    api.updateUser({
        "email": req.params.email,
        "verificationID": req.params.ver_id
    }, req.body, function(err, result) {
        if (err) {
            console.log("error is :", err);
            res.status(400).send(err);
        } else {
            if (result == 1) {
                res.send({"updated" : 1});
            }

        }


    });
});

/*
 * @name POST /userS
 * @desc
 * To get all the users from database
 */
router.post('/list', function(req, res, next) {
    console.log("getting all users ..", req.body);
    api.getUsers(req.body.member, function(err, result) {
        if (err) {
            console.log("error in getting all users is :", err);
        } else {
            console.log("the hwljfgdk:", result);
            res.send(result);
        }
    });
});

/*
 * @name PUT /reset
 * @desc
 * To reset the password of user
 */
router.put('/reset/:email', function(req, res, next) {
    console.log("email is :", req.params.email);
    console.log("updation is :", req.body);
    api.updateUser({
        "email": req.params.email
    }, req.body, function(err, result) {
        if (err) {
            console.log("error is :", err);
            res.status(400).send(err);
        } else {
            console.log("succesful password updation :", result);
            api.login({
                "email": req.params.email,
                "pass": req.body.pass
            }, function(err, resu) {
                if (err) {
                    console.log("error with new pass with :", err);
                    res.send(err);
                } else {
                    console.log("now the user is : ", resu[0].email);
                    smtpTransport.sendMail({
                        from: "teamtaskhandling@gmail.com", // sender address
                        to: resu[0].email, // comma separated list of receivers
                        subject: "Password Reset", // Subject line
                        text: "New password for your account is given below. \n\n" + resu[0].pass // plaintext body
                    }, function(error, response) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("Message sent: " + response);
                            res.send(response);
                        }
                    });

                }
            });
        }
    });
});

/*
 * @name POST /login
 * @desc
 * To validate login 
 */



router.post('/login', function(req, res, next) {
    console.log('index.' + JSON.stringify(req.body));

    api.login(req.body).then(function(value) {
        console.log(".....oops", value);
        res.send(value);
    });
});

/*
 * @name GET/user
 * @desc
 * To verify user account
 */

router.get('/find/:verId/:emailID', function(req, res, next) {
    console.log("what i got is :" + req.params.verId + "and    " + req.params.emailID);
    api.getUser({
        "email": req.params.emailID,
        "vcode": req.params.verId
    }, function(err, result) {
        if (err) {
            console.log("error is :", err);
        } else {
            console.log("succesful", result);
            res.send(result);
        }
    });
});



/*
 * @name POST/register
 * @desc
 * To register a new user with his details
 */

router.post('/register', function(req, res, next) {
    res.setHeader("Content-Type", "application/json");
    console.log("there is register");
    api.register(req.body, function(err, result) {
        if (err) {
            console.log("there is error in insertion", err);
            err = {
                "name": err
            };
            res.status(400).send(err);
        } else {
            console.log('result register:' + JSON.stringify(result));
            var userHtml = "<b> Hello <%= name %> </b><br>You have successfully registered for <b>Kanban Task Manager</b>.Your account information is :<br><br><div style='border:1px solid blue;padding:5px;'>Email-Id : <%= email %> <br>Password : <%= pass %><br><br>Please verify following link to verify your email-id <br > http://192.168.100.88:4000/#/verify/<%= verificationID %>/email/<%= email %>";
            var user = {};
            user["html"] = userHtml; //nodemailer support both array and comma separated string for to, bcc and cc
            user["data"] = result;
            user["to"] = [result.email];
            user["subject"] = "Kanban Registration";
            user["type"] = "nodemailer"; // can be amazon/nodemailer/sendgrid

            mailservice.sendMail(user, function(err, data) {
                console.log("mail success ....", data);
            });
            res.send({
                "success": 1
            })
        }

    });
});

/*
 * @name POST /image
 * @desc
 * To upload image on the server
 */

router.post('/image', function(req, res, next) {
    var user_Id;
    var sendPath = "";
    console.log("thisi s image post .....");
    req.pipe(req.busboy);

    req.busboy.on('file', function(fieldname, file, filename) {
        fieldname = user_Id;
        console.log("fieldname...", fieldname);
        var savePath = path.join('/home/deepak/Deepak Trello/myapp/client/images', fieldname, filename);
        mkdirp.sync(path.dirname(savePath));
        file.pipe(fs.createWriteStream(savePath));
        sendPath = fieldname + "/" + filename;
        console.log("sendpath", sendPath);
        res.send({
            "picPath": sendPath
        });
    });

    req.busboy.on('field', function(fieldname, val) {
        user_Id = JSON.parse(val).emailId;
    });

    console.log(req.body);
    console.log(req.file);
    console.log("sendpath", sendPath);
});


router.get('/getSingleUser/:emailID', function(req, res, next) {
    console.log("what i got is :" +  req.params.emailID);
    api.getSingleUser({
        "email": req.params.emailID
        
    }, function(err, result) {
        if (err) {
            console.log("error is :", err);
        } else {
            console.log("succesful", result);
            res.send(result);
        }
    });
});

router.put('/updateuser',function(req,res ,next) {
    api.updateUser({
        "email": req.body.email
        
    }, {"pass" : req.body.pass}, function(err, result) {
        if (err) {
            console.log("error is :", err);
            res.status(400).send(err);
        } else {
            if (result == 1) {
                res.send({"updated" : 1});
            }

        }


    });
});

module.exports = router;
