var mongoose = require ('mongoose');
// var ObjectID = mongoose.mongo.BSONPure.ObjectID;
mongoose.connect ('mongodb://localhost/trello');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var userInfo = new Schema ( {
  name:{type:String},
  email:{type:String, unique:true , required:true},
  pass:{type:String, unique:true , required:true},
  verified : {type: Boolean}, 
  verificationID : {type:String},
  userImage : String,
  lastName : {type:String},
  designation : {type:String},
  contact : {type:String},
  skypeId: { type:String }  
 
},{ collection : "user" } );

userInfo.plugin(uniqueValidator);
module.exports.trello1=mongoose.model('trello1', userInfo);

var projInfo = new Schema ( {
    userEmail :{type: String},
    name:{type:String , required:true },
    description:{type:String },
    members : {type:Array},
    
}, { collection:"project" } );

projInfo.plugin(uniqueValidator);
module.exports.trello2=mongoose.model('trello2', projInfo);

var taskInfo = new Schema ( {
  projName : { type:String },
  userEmail : { type: String },
  title : { type:String, required:true },
  comment : [ {
          user_email : String,
          comment : String
        }
  ],
  completion_date : Date,
  taskStatus : {type:String, required:true},
  description : {type:String },
  seq_no : { type: Number },
  assignedTo : { type:String },
  timeEstimated: { type: Number }

},{ collection:"tasks" });

taskInfo.plugin(uniqueValidator);

module.exports.trello3 = mongoose.model('trello3',taskInfo);