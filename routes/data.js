var express = require("express");
var db=require("./trello.js");
var Q = require('q');


module.exports.api = {

 readExample : function(str){
  console.log("in th readExample..");
  return str;
 },





  /*
    * @name login
    * @desc
    * To find and return the details of logged-in user 
  */


	login : function(opts){
    console.log("opts.user is :",opts.email);
    console.log("opts.pass is :",opts.pass);
    var d = Q.defer();
	   db.trello1.find({email : opts.email, pass:opts.pass},function(err,result){
     if(err){ d.reject(err);}
     else {d.resolve(result[0]);}
    console.log("...deepak ",result);
     
    });
     
     return d.promise;
   
  	
  },
  /*
    * @name getUsers
    * @desc
    * To find and return the details of all users
  */
  getUsers : function(opts, callback) {
   console.log("in the find of getuesrrss...",opts);
   db.trello1.find({},function(err, result) {
      if(err) {
        console.log(err);
        callback(err,null);
      } else {
         console.log("users without mebers are :",result);
         callback(null,result);
      }
   });
  },
  /*
    * @name getUser
    * @desc
    * To find and return the details of user to be verified
  */
  getUser : function (opts,callback) {
     console.log("opts.email is :" ,opts.email);
     console.log("opts.verifictaion code is :" ,opts.vcode); 
     db.trello1.find({email : opts.email},function(err,res) {
      if(err ) {
        callback(err,null);
      } else  { 
        console.log("the found user is :",res.verified);
        callback(null,res); 
      }

   });
  },
  /*
    * @name getOwner
    * @desc
    * To find and return the owner of a task 
  */
  getOwner : function(opts,callback) {
    console.log("what  i get is :..." ,  opts);
    db.trello1.find({_id:opts},function(err,res) {
       if(err) {
        callback(err,null);
       } else {
        callback(null,res);
       }
    });
  },

  getSingleUser : function(opts,callback) {
    console.log("what  i get is :..." ,  opts);
    db.trello1.findOne(opts,function(err,res) {
       if(err) {
        callback(err,null);
       } else {
        callback(null,res);
       }
    });
  },
  /*
    * @name register
    * @desc
    * To create a new user and returms its details
  */
  register : function( opts,callback) {
    console.log("opts:" + JSON.stringify(opts)); 

    db.trello1.create(opts, function(err, todo){
    if (err) {
    	console.log("mongoerror is :",err);
    	callback(err.errors.email.path,null);
    }  else { 
    	console.log("inserted");
      callback(null,todo);
    }
     });
    },
  /*
    * @name addProject
    * @desc
    * To create a new project and returns its details
  */
  addProject : function(projectInfo,callback) {
    console.log("adding an project");

    db.trello2.create(projectInfo,function(err,res){
      if (err) {
    	 console.log(err);
    	 callback(err,null);
      } else {
        console.log("project added");
    	 callback(null,res);
      }
    }
   
   );
  },
  /*
    * @name login
    * @desc
    * To find and return the details of logged-in user 
  */
  getProject : function(projInfo, callback) {
    console.log("...........................",projInfo);
  	db.trello2.find({"members" :projInfo.userEmail}, function(err,res) {
  		 if (err) {
  		 	console.log(err);callback(err,null);
  		 }  else {
  		 	console.log("result is :" , res);callback(null,res);
  		 }
  	}
       
  	);
  },


  get_single : function (options , callback) {
    console.log("options are :",options)
    db.trello2.find(options ,function (err , res) {
      if(err) callback(err,null);
      else callback(null , res);
    })
  },
  /*
    * @name addTask
    * @desc
    * To create a new task for a project and return its details
  */
  addTask : function(taskInfo,callback) {	
    console.log(JSON.stringify(taskInfo));
     
    db.trello3.create(taskInfo,function(err,res){
      if (err) {
      	console.log(err);
      	callback(err,null);
      } else {
      	console.log("task added");
        callback(null,res);
      }
    }
   
   );
  },
  /*
    * @name getTask
    * @desc
    * To find and return the details of all tasks
  */
  getTask : function(tasks, callback) {
  	db.trello3.find({$query :tasks,$orderby : {seq_no : 1}} ,function(err,res) {
  		 if (err) {
  		 	console.log(err);
  		 }  else {
  		 	console.log(res);callback(null,res);
  		 }
  	}
       
  	);
  },
  /*
    * @name login
    * @desc
    * To find and return the details of a task
  */
  getTask1 : function(tasks, callback) {
    console.log("thisiss ss", tasks);
    db.trello3.find(tasks ,function(err,res) {
       if (err) {
        console.log(err);
       }  else {
        console.log(res);callback(null,res);
       }
    }
       
    );
  },
  /*
    * @name updateUser
    * @desc
    * To update the details of user 
  */
  updateUser : function(userToUpdate, updation, callback) {
    console.log("user to update is ", userToUpdate);
    console.log("updation is :", updation)
    db.trello1.update(userToUpdate,{$set: updation},function(err, res) {
      if (err) {
        console.log(err);
        callback(err,null);
      } else {
        console.log("user updated",res);
        callback(null,res);
      }
    });
   },
   /*
    * @name updateProject
    * @desc
    * To update the details of a project
  */
  updateProject : function(projToUpdate, updation, callback) {
    try {
      if (!projToUpdate.name){
        throw "this is an err";
      }
      else
      {
        db.trello2.update(projToUpdate,updation,function(err,result) {
        if (err) {
        console.log(err);
        callback(err,null);
        }   else {
        console.log("project updated");
        callback(null,result);
        
        }
       });

      }
    }
      catch(err){
          err.prototype=Error.prototype;
        callback(err,null);
      }  
  },
  /*
    * @name updateProject1
    * @desc
    * To add member to an project 
  */
  updateProject1 : function(projToUpdate, updation, callback) {
    try {
      if (!projToUpdate.name){
        throw "this is an err";
      }
      else
      {  console.log("user email is :",projToUpdate.userId);
        console.log("proje name  is :",projToUpdate.name);
        console.log("user email is :",updation.memberEmail);
        db.trello2.update(projToUpdate,{$push : {"members":updation.memberEmail} },function(err,result) {
        if (err) {
        console.log(err);
        callback(err,null);
        }   else {
        console.log("project updated");
        callback(null,result);
        
        }
       });

      }
    }
      catch(err){
          err.prototype=Error.prototype;
        callback(err,null);
      }  
  },
  /*
    * @name updateTask
    * @desc
    * To update the details of the task
  */
  updateTask : function(taskToUpdate,updation, callback) {
  	try {
  		if (!taskToUpdate.title){
  			throw "this is an err";
  		}
  		else
  		{  console.log("......", taskToUpdate);
        db.trello3.update(taskToUpdate,updation,function(err,result) {
  		if (err) {
  			console.log(err);
        
  			callback(err,null);
  		}   else {
  			console.log("task updated");
        callback(null,result);
  			
  		}
  	});

  		}
  	}
  		catch(err){
  			  err.prototype=Error.prototype;
  			callback(err,null);
  		}
  	
  },
  /*
    * @name deleteTask
    * @desc
    * To delete a task 
  */
 deleteTask : function(taskToDelete,callback) {
  	db.trello3.remove(taskToDelete,function(err,result) {
  		if (err) {
  			console.log(err);
  			callback(err,null);
  		}   else {
  			callback(null,result);
  			console.log("task deleted");
  		}
  	});
  }
   
}