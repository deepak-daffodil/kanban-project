var express = require('express');
var router = express.Router();
var api=require('./data.js').api;
var nodemailer = require("nodemailer");
var fs=require('fs-extra');
var path =require('path');
var mkdirp = require('mkdirp');
var smtpTransport = nodemailer.createTransport();
var Q = require('q');

/*
  * @name GET/project
  * @desc
  * To get aal the projects of a user
*/

router.get('/list/:userEmail', function(req, res, next) {
  res.setHeader("Content-Type","application/json");
  api.getProject({"userEmail" : req.params.userEmail},function(err, result) {
    if (err) {
      if(err[0].code == 11000) {
          res.status(400).send(err);console.log("errorrrr....")
      } } else {
        console.log('all the projects..............')
        res.send(result);
      }
    
  }); 
   
});

router.get('/this/:proj_name/:email' , function(req,res , next) {
  console.log("req.params ar e:...." , req.params);
  api.get_single({"userEmail" : req.params.email , "name" : req.params.proj_name} , function (err ,result) {
    if(err) {
      console.log("yo to error aa gya bhai",err);
      res.status(404).send(err);
    } else {
      console.log("dekh yo aala paya se .... => ");
      res.send(result);
    }
  })
})

/*
  * @name POST /project
  * @desc
  * To create a new project for the user
*/

router.post('/add',function(req,res,next) {
     console.log("/project :" +JSON.stringify(req.body));
     api.addProject(req.body,function(err,result){
      if(err){
        res.status(400).send(err);
      } else {
        console.log('add project' +JSON.stringify(result));
        res.send(result);
      }
     });
});

/*
  * @name PUT /project
  * @desc
  * To update the details of project
*/

router.put('/update/:pName/:userid',function(req,res,next) {
 console.log("/project :" +JSON.stringify(req.body));
 api.updateProject({"name" : req.params.pName ,"userId": req.params.userid},req.body,function(err,result){
  if(err){ 
    console.log("error in updation is :" , err);
    res.status(400).send(err);
  } else { 
     console.log('updated project' +JSON.stringify(result));
     api.getProject({"userId" : req.params.userid},function(err, result) {
       if (err) { console.log("errorrrr....");
         res.status(400).send(err);
       } else {
         console.log('all the taks..............');
         res.send(result);
       }

     });
   }
 });
});

/*
  * @name GET/member
  * @desc
  * To get the details of project member individually
*/

router.get('/member/:emailID',function(req,res,next){
  console.log("what i got is :" + req.params.emailID);
  api.getUser({"email": req.params.emailID},function(err,result){
     if(err) { console.log("error is :",err);}
     else {console.log("succesful",result);res.send(result);}
  });
});
module.exports = router;
