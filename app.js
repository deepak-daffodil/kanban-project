var express = require('express');

var busboy = require('connect-busboy');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs-extra');
var routes = require('./routes/index');
var users = require('./routes/user_requests.js');
var project = require('./routes/project_requests.js');
var task = require('./routes/task_requests.js');
var app = express();
var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});

// app.use(express.logger({stream: logfile}));

app.use(busboy({immediate:false}));
app.use(express.static(__dirname + '/client'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/user',users);
app.use('/project',project);
app.use('/task',task);

app.post('/create_file', function(req, res){
  log_file.write(req.body.name + "\n");
  res.send({"ok":1})
});
app.use('/', routes);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
  });
}
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('err', {
    message: err.message,
    error: {}
  });
});
app.listen(4000);
console.log("Server started at port 4000...");
module.exports = app;