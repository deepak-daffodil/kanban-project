kanbanApp.controller("dashboardController", function($scope, $http, $state, DataService, $cookieStore) {
    if ($cookieStore.get('userCookie'))
        $scope.userCookie = $cookieStore.get('userCookie');
    else
        $state.go("login");
    if ($scope.userCookie.name.indexOf(" ") > 0 || $scope.userCookie.name.indexOf("-") > 0 || $scope.userCookie.name.indexOf("_") > 0)
        $scope.named = $scope.userCookie.name.substr(0, $scope.userCookie.name.indexOf('-'));
    else
        $scope.named = $scope.userCookie.name;
    $scope.userEmail = $scope.userCookie.email;

    $scope.getAllProjects = function() {
        DataService.getWebService($scope, '/project/list/' + $scope.userCookie.email, function(err, data) {
            if (err) {
                console.log("the erroe is ", err);
            } else {
                $scope.projects = data;
                $scope.member_projects = [];
                $scope.owner_projects = [];
                for (var i = 0; i < data.length; i++) {
                    if ($scope.projects[i].members[0] == $scope.userCookie.email) {
                        $scope.owner_projects.push($scope.projects[i]);
                    } else {
                        $scope.member_projects.push($scope.projects[i]);
                    }
                }

            }
        });
    };

    $scope.callProject = function() {
        $scope.form = {
            userEmail: $scope.userCookie.email,
            members: $scope.userCookie.email,
            name: $scope.projname
        };
        DataService.postWebService($scope, '/project/add', $scope.form, function(err, data) {
            if (err) {
                console.log("error in project post call: ", err);
            } else {
                $state.go("dash.project", {
                    "project_name": data.name
                });
            }
        });
    };

    $scope.showTasks = function(index) {
        $scope.projectnamed = index.name;
        $scope.thisProject = index;
        $scope.list = false;
        $state.go("dash.project", {
            "project_name": index.name
        })
    };



    $scope.inviteNow = function(invite) {
        console.log("... in side ", invite);
        var invitePass = Math.random().toString(36).slice(-8);
        var inviteVerID = Math.random().toString(36).slice(-8);
        if ($scope.allmembers.indexOf(invite) == -1) {
            DataService.postWebService($scope, '/user/register', {
                projId: $scope.projectnamed,
                userEmail: $scope.userCookie.email,
                name: "Your Name",
                email: invite,
                pass: invitePass,
                verificationID: inviteVerID,
                verified: false
            }, function(err, data) {
                if (err) {
                    console.log("error in inviting member is: ", err);
                } else {
                    console.log("data is :", data);

                }
            })
        } else {
            console.log("User is already registered........");
        }
    };

    $scope.showUserDetails = function() {

        $state.go("userprofile", {
                "user_email": $scope.userCookie.email
            })
            // $scope.userdetails = true;
            // $scope.userFName = $scope.named;
            // $scope.userContact = $scope.userCookie.contact;
            // $scope.userLName = $scope.userCookie.name.substr($scope.userCookie.name.indexOf(' ')+1);
            // $scope.userPost = $scope.userCookie.designation;
            // $scope.userSkypeId = $scope.userCookie.skypeId;
            // $scope.thisPath = $scope.userCookie.userImage;
    };
    $scope.logout = function() {
        $cookieStore.remove('userCookie');
        console.log("cookies is:", $cookieStore.get('userCookie'))
        $state.go("login");
    };

}).directive('projectslist', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/projectslist.html'
    };
}).directive('projectdetails', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/projectdetails.html'
    };
}).directive('addprojects', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/addprojects.html'
    };
}).directive('userview', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/userview.html'
    };
}).directive('userdetails', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/userdetails.html'
    };
}).directive('headers', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/headers.html'
    };
});
