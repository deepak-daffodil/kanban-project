kanbanApp.controller('profileController', function($scope, $http, $location, $state, $cookieStore, $stateParams, DataService) {
    console.log("email of user is:", $stateParams.user_email);
    if ($cookieStore.get('userCookie'))
        $scope.userCookie = $cookieStore.get('userCookie');
    else
        $state.go("login");
    DataService.getWebService($scope, "/user/getSingleUser/" + $stateParams.user_email, function(err, data) {
        if (err) {
            console.log("error in getting the single users ", err);
        } else {
            console.log("single user details are:", data);
            if (data) {
                $scope.userFName = data.name;
                $scope.userEmail = data.email;
                $scope.userLName = data.lastName;
                $scope.userPost = data.designation;
                $scope.userContact = data.contact;
                $scope.userSkypeId = data.skypeId;
                $scope.userDetails = true;
            }
        }
    });
    $scope.userUpdate = function() {
        $scope.form = {
            name: $scope.userFName,
            lastName: $scope.userLName,
            designation: $scope.userPost,
            contact: $scope.userContact,
            skypeId: $scope.userSkypeId,
            userImage: $scope.thisPath,
            email : $scope.userEmail
        };
        console.log("user updatedion data is :",$scope.form	);
        DataService.putWebService($scope, "/user/updateuser/" , $scope.form , function(err , data) {
        	if(err) {
        		console.log("putting the error is :",err);
        	} else {
        		console.log("data is :",data);
        	}
        })


    };
});
