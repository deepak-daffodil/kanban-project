// var kanbanApp  =  angular.module('kanbanApp', ['ui.router','ngCookies' , 'ang-drag-drop','ngAnimate','angularFileUpload','ngDialog']);
var userVerId="";
var seq =0;

kanbanApp.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  // HOME STATES AND NESTED VIEWS  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = 
  .state('login', {
    url: '/login',
    templateUrl: '/login/login.html',
    controller: 'loginController',
  })
  .state('reset', {
    url: '/reset-password',
    templateUrl: '/login/reset.html',
    controller: 'resetController',     
  })
  .state('/',{
    url:'/',
    templateUrl: '/login/login.html',
  })        
  .state('dash', {
    url: '/home',
    templateUrl: '/user/dash.html',
    controller: 'dashBoard',       
  })
  .state('change', {
    url: '/user/change',
    templateUrl: '/registration/change.html',
    controller: 'changeThePassword',
  })
  .state('message', {
    url: '/register/success',
    templateUrl: '/registration/message.html',
    controller: 'successController',        
  })
  .state('verify', {
    url: '/verify/:ver_id/email/:email_id',
    templateUrl: '/login/verify.html',
    controller: 'verifyController',     
  })
  .state('register', {
    url: '/register',
    templateUrl: '/registration/reg.html',
    controller: 'registerController',
  });
  $urlRouterProvider.otherwise('/');
});








kanbanApp.controller('resetController',['$scope','$http','$state','$cookieStore',function($scope,$http,$state,$cookieStore) {
   $scope.resetIt=true;

   $scope.createPassword = function() {
    var x = Math.random().toString(36).slice(-8);
    console.log("password is :",x);
    $http.put('/user/reset/'+$scope.resetEmail, {pass:x}).
      success (function(data) {
        console.log("this is new user password:", data);
        alert("New PAssword has been sent to your email-id. \\n Please check your email-id.");
        
          $state.go("login");
        
     });
   }   
  }   
]);
var inc = 0.0001;
var count=0;

kanbanApp.controller('dashBoard' ,[ '$scope','$upload','$http','$location','ngDialog','$cookieStore' ,'$state',function($scope ,$upload, $http,$location,ngDialog, $cookieStore, $state) {
          
         // $scope.progress=[]; 
         $scope.y= $cookieStore.get('userCookie');


         $scope.userDetails=[];
         $scope.project=false;
         $scope.back1 = true;
         $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;
         $scope.userdetails = false;
         $scope.allshowhide = ["back1","list","anything","edit",
                               "popup","rename","addmember","memberdetails"
                               ,"members","userview","userdetails"];


         var z;
         $scope.addmembers = [];
         $scope.thisProject = {};         
         $scope.projectnamed="";
         $scope.name="";
         $scope.hello="";
         if($scope.y.name.indexOf(" ") >0)
         $scope.named = $scope.y.name.substr(0,$scope.y.name.indexOf(' '));
         else
          $scope.named = $scope.y.name;
         console.log( "names is ",$scope.named);
         $scope.userEmail = $scope.y.email;
         console.log();

         //$scope.getProject1();
         
         

        $scope.change = function(id){
          console.log($("#span"+id));
          if($('#span'+id).hasClass('glyphicon-edit')){
          $('#span'+id).attr('class' ,'paddingRemove glyphicon glyphicon-ok-sign  edit');
          $('#task'+id).removeAttr('readonly');
          $('#task'+id).focus();
         }
         else
         {
          $('#task'+id).attr('readonly','readonly');
          $('#span'+id).attr('class' ,'paddingRemove glyphicon glyphicon-edit  edit')
         }
   
        };

          $scope.onFileSelect = function($files) {
          $scope.upload = $upload.upload({
            url: '/user/image', 
            method: 'POST',
            data: {emailId:$scope.y.email},
            file:$files[0], 
        }).progress(function(evt) {
            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
        }).success(function(data, status, headers, config) {
            console.log("what i got is ",data);
            $scope.thisPath = '/images/'+data.picPath;
        });
    
        },
         $scope.member_projects = [];
         $scope.owner_projects = [];
         $scope.getProject1 = function () {
            $http.get('/project/list/'+$scope.y.email , {}).
                        success ( function(data) {
                          console.log("all the projects are:", data);
                          $scope.projects = data;
                          $scope.member_projects = [];
                          $scope.owner_projects = [];
                          for(var i =0;i<data.length;i++)
                          {
                            if($scope.projects[i].members[0]==$scope.y.email)
                              { $scope.owner_projects.push($scope.projects[i]); }
                            else{
                                $scope.member_projects.push($scope.projects[i]);
                            }
                          }
                          
                          console.log($scope.allmembers);


                        });
         };
        $scope.getProject1();

         $scope.toggle = function () {
            $scope.list = !$scope.list;
            $scope.anything=false;
            $scope.edit=false;
            $scope.popup=false;
            $scope.addT=false;
            $scope.rename=false;
            $scope.addmember = false;
            $scope.memberdetails= false;
            $scope.members=false;
            $scope.userview= false;
            $scope.userdetails = false;
            if($scope.project == true)
            {
              $scope.apply2 = "changeClass1";
            }
            
            
         };
          $scope.logout = function() {
            $cookieStore.remove('userCookie');
            console.log("cookies is:" , $cookieStore.get('userCookie'))
            $state.go("login");
          };
         $scope.editTask = function() {
          $scope.edit = true;
          $scope.list=false; 
        
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;

         };
         $scope.updateTask = function() {
           $scope.form={title:$scope.thistaskName, description : $scope.thistaskDesc,
            taskStatus : $scope.thistaskStatus , completion_date : $scope.thistaskDate ,
            assignedTo : $scope.thistaskAssignedTo , timeEstimated : $scope.thistaskDays};
            $http.put('/task/update/'+$scope.hello+'/'+$scope.thisProject._id,$scope.form).
               success( function(data) {
                  console.log("updated");
                  console.log("udtaion is :" , data);
                  
                    var ind = $scope.tasks.indexOf(data[0]);
                    $scope.tasks.splice(ind,1);
                    console.log("task deleted");
                    $scope.getTask1();
                   $scope.anything = false;
               });
         };

         $scope.show = function () {
           $scope.popup =!$scope.popup;
           $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
       
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;
           if($scope.project == true)
            {
              $scope.applyThis = "changeClass";
            }
         };

         $scope.callProject = function () {
          console.log("hellow")
            $scope.form= { userId : $scope.y._id ,members: $scope.y.email, name:$scope.projname , description:$scope.projdesc};
            $http.post('/project/add' , $scope.form).
              success( function(data){
                        console.log(data);
                        console.log("new project added");
                        $scope.project = true;
                        $scope.thisProject = data;
                        $scope.projectnamed= data.name;
                        z=data;
                        $scope.popup=false;
                       $scope.getProject1();
                       $scope.showTasks(z);
            });
         };
         $scope.taskshow = function() {
          $scope.addT=!$scope.addT;
          $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;
         };
         $scope.showTasks = function(index) {
              $scope.project = true;
              $scope.allmembers=index.members;
              $scope.projectnamed= index.name;
              $scope.thisProject = index;
              $scope.projectNames = angular.copy($scope.projectnamed );
              console.log("hu..........",$scope.projectNames);
              //console.log("index is:" , $scope.projectnamed);
              console.log("....................",$scope.thisProject);
               // z=index;
              $scope.getTask1();
              $scope.list=false; 
          };
        
         $scope.addTask1 =function () {
           console.log("add taskkkk");
           seq+=1;
           $scope.form = { projId: $scope.thisProject._id, userId : $scope.y._id,title : $scope.taskname , taskStatus :"backlog" ,
                        seq_no :(($scope.tasks.length)+1)* 10000};
           $http.post('/task/add' , $scope.form).
              success( function(data){
                       console.log(data);
                       console.log("new task added");
                       $scope.getTask1();
                       $scope.taskname ="";
                        
            });
         };
         
         $scope.getTask1 = function () {
           console.log("in get task:" , $scope.projectnamed);
           $http.get('/task/list/'+$scope.thisProject._id , {}).
                        success ( function(data) {
                        console.log("all the tasks are:", data);
                        $scope.tasks = data;

                          // $scope.tasks.splice(0);
                          // $scope.progress.splice(0);
                          // for(var i=0;i<data.length;i++) {
                          //    if (data[i].taskStatus=="backlog") {
                          //     $scope.tasks.push(data[i]);
                          //     } else if (data[i].taskStatus=="inprogress") {
                          //       $scope.progress.push(data[i]);
                          //     }
                          // }
                          // console.log("tasks is :" , $scope.tasks);
                          // console.log("progress is :" , $scope.progress);
                       });
                          
         };
         $scope.taskDetails1 = function(para) {
            
            $scope.anything = true;
            $scope.edit = false;
            $scope.list=false; 
            $scope.popup=false;
            $scope.addT=false;
            $scope.rename=false;
            $scope.addmember = false;
            $scope.memberdetails= false;
            $scope.members=false;
            $scope.userview= false;
            $scope.hello=para.title;
            $scope.thistaskName = para.title;
            $scope.thistaskStatus= para.taskStatus;
            $scope.thistaskDays = para.timeEstimated;
            $scope.thistaskAssignedTo = para.assignedTo;
            console.log("in tsk details.........");
            $scope.allComments = para.comment;
            console.log("devvvvvvvv",$scope.taskcomment);
            

         };
          $scope.projectname = "";
         $scope.renameProject1 = function () {
        // $scope.rename = false;
        console.log("in the rename project : " , $scope.projectnamed);
        $http.put('/project/update/'+$scope.projectname+'/'+$scope.y._id, {name:$scope.projectnamed}).
        success ( function(data) { 
            console.log("project is updated..." ,data);
            $scope.getProject1();

            
            // $scope.getTask1();
            $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;
            // $scope.projectnamed
        });
      };
         $scope.rename1 = function () {
          if($('#editProject').hasClass('glyphicon-edit')){
            $('#editProject').attr('class','paddingRemove glyphicon glyphicon-ok-sign  edit');
           $('#entername').removeAttr('readonly');
            $('#entername').focus();
            $scope.projectname = $scope.projectnamed;
          }
          else
          {
              $scope.renameProject1();
              $('#editProject').attr('class','paddingRemove glyphicon glyphicon-edit  edit')
          }

         $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;
         };
      
       



      var c=0;
      $scope.showMembers = function () {
        if(c==0) {
        $scope.members = true;c=1;
        } else if(c==1) {
          $scope.members = false;
          c=0;
        }
        var randomColor ="e6e6e6";
        
       for(var i =0;i<$scope.allmembers.length;i++) {
        console.log("hello.................",document.getElementById(i).style.cursor);
         var randomColor = Math.floor(Math.random()*16777215).toString(16);
          if(document.getElementById(i).style.backgroundColor=="red")
            { document.getElementById(i).style.color = "white";}
          else{
        document.getElementById(i).style.color = "#"+randomColor;
         }
       }
        
        // }

        $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.userdetails = false;
         $scope.userview= false;
      };


      
      $scope.addMembers = function() {
        // console.log($scope.allmembers);
        $scope.addmembers =[];
        $http.post('/user/list',{ "member": $scope.allmembers} ).
        success(function(data) {
          console.log(data);
          $scope.lengthof = data.length;
          for(var j=0;j<data.length;j++){
          $scope.addmembers.push(data[j]);}
          console.log("the array is:", $scope.addmembers);
          // $scope.myStyle={opacity:0.4;};
          $scope.addmember=true;
          $scope.userview=false;
          // angular.element( document.querySelector( '#allthedivs' ) ).opacity=0.4;
          // getElementsByTagName("allthedivs").style.opacity=0.4;
        }); 
      };
      
     
      $scope.showMemberDetails = function (member) {
        $scope.memberdetails= !$scope.memberdetails;
        $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.userdetails= false;
         $scope.members=false;
         $scope.userview= false;
        console.log("email id sent is :",member);
        $http.get('/project/member/' + member,{})
        .success( function (data) {
          console.log("member  s:" , data);
          $scope.memName = data[0].name;
          $scope.memEmail = data[0].email;
          $scope.thisPath = data[0].userImage;
        });

      };

      $scope.joinMember = function(member) {
        console.log("thi is :",$scope.addmembers.indexOf(member));
        $scope.allmembers.push(member.email);
        $scope.addmember = true;
       
          $http.put('/project/update/'+$scope.projectnamed+'/'+$scope.y._id, {members:$scope.allmembers}).
        success ( function(data) { 
            console.log("project is updated..." ,data);
            $scope.getProject1();

            
            // $scope.getTask1();
           
            // $scope.projectnamed
        })

      };
      var colorcount=0;
      $scope.somethingExtra = function(id){
        console.log("id is ...", id);
        if(colorcount==0){
        var colorarray =["red","green","blue","brown","bisque","blueviolet","lawngreen","crimson","gold","indianred"];
        console.log("and ..",document.getElementById("name"+id).style.border);
        document.getElementById("name"+id).style.color = colorarray[id];
        document.getElementById("name"+id).style.fontSize='120%';
        document.getElementById("name"+id).style.fontWeight='bolder';
        colorcount+=1;
        }
      }
      $scope.somethingExtra1 = function(id){
        colorcount=0;
        document.getElementById("name"+id).style.color = "black";
        document.getElementById("name"+id).style.fontSize='12px';
        document.getElementById("name"+id).style.fontWeight='bold';
      }
      
    var spliceflag = true;
    $scope.sendMail = function($data) {
      console.log(".this is ending mil ",$data);
    // router.post('/completed',)
    };
     
    $scope.dropSuccessHandler = function($event,index,indo,array){
       if(spliceflag) {
      array.splice(index,1);
    }
    };
   
    $scope.onDrop = function($event,index,$data,indo,array){
      spliceflag = true;
      var thisid = $event.target.id;
      var dodrop = true;
      if(indo=="inprogress" || indo=="completed"||thisid == "progress1" || thisid == "complete1")
      { 
        if(!$data.timeEstimated){ 
        alert("Please Give an Estimation Time to move the task..");
        // array.push($data);
        spliceflag = false;
        dodrop= false;

       }          
      }
      if(dodrop) {
        console.log("..............",thisid);
       if(thisid=="backlog1" ) {
        $data.taskStatus = "backlog";
        count=2;
       // $scope.back1 = false;
        } else if (thisid=="progress1") {
         $data.taskStatus = "inprogress";count=2;
         //$scope.prog1 = false;
         } else if(thisid=="complete1") {
        $data.taskStatus="completed";count=2;
        // $scope.comp1= false;
         } else {
        $data.taskStatus = indo;
        count=1;
         }

      if(thisid=="complete1" || indo=="cmpleted"){
        $scope.date = new Date();
        $scope.date1 = new Date($scope.date.getFullYear(),$scope.date.getMonth(),$scope.date.getDate());
        console.log("...................",$scope.date1);
        $data.completion_date = $scope.date1;
        console.log("this is s",$data);
        $http.post('/completed/'+$scope.y.email,$data)
        .success(function (data) {
         console.log("sent mail....");
        });
      }
      
      if(count == 1)
      {      
       var newind =-1;
       if($data.taskStatus == indo) {
        console.log("coming data is :" , $data);
        for(var j=index-1; j>0; j++) {
          if(array[j].taskStatus==indo) {
             newind = j;
             console.log(j);
             break;
          } 
        }
       // if (index==0 || newind == -1) {
       //   array[index].seq_no = array[index].seq_no /2;
       //   $data.seq_no = array[index].seq_no / 2 + inc;
       //   } else {
       //     $data.seq_no = array[index-1].seq_no + (array[index-1].seq_no/2) + inc;
       //   }
          $scope.targetElement = array[index];
          $data.seq_no = array[index].seq_no -1;
          array[index].seq_no = array[index].seq_no + index + 100;
          // inc +=1;
         }
         // console.log("target data is :", array[index].seq_no);
         count = 0;
         }
         array.push($data);
         
         $http.put('/task/update/'+$data.title+'/'+$scope.thisProject._id,$data).
               success( function(data) {
                  console.log("updated");
                  console.log("udtaion is :" , data);
                  
                    var ind = $scope.tasks.indexOf(data[0]);
                    $scope.tasks.splice(ind,1);
                    console.log("task deleted");
                    $scope.getTask1();
                   $scope.anything = false;
               });
       // }
       if(!count==2){
         $http.put('/task/update/'+$scope.targetElement.title+'/'+$scope.thisProject._id,$scope.targetElement).
               success( function(data) {
                  console.log("updated");
                  console.log("udtaion is :" , data);
                  
                    var ind = $scope.tasks.indexOf(data[0]);
                    $scope.tasks.splice(ind,1);
                    console.log("task deleted");
                    $scope.getTask1();
                   $scope.anything = false;
               });
       } 
      }// }
       
    };
    
    $scope.yourView = function () {
      $scope.userview =!$scope.userview;
      $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         
      $scope.userdetails = false;
      $scope.userEmail = $scope.y.email;
      $scope.thisPath = $scope.y.userImage;
      $scope.userSkypeId = $scope.y.skypeId;
      $scope.userContact = $scope.y.contact;
      console.log("ddvdvd", $scope.userview);
      if($scope.project) {
        $scope.apply3 = "changeClass2";
      }
    };
    
    $scope.showUserDetails = function() {
      $scope.userdetails = true;
      $scope.list=false; 
         $scope.anything=false;
         $scope.edit=false;
         $scope.popup=false;
         $scope.addT=false;
         $scope.rename=false;
         $scope.addmember = false;
         $scope.memberdetails= false;
         $scope.members=false;
         $scope.userview= false;;
      $scope.userFName = $scope.named;
      $scope.userContact = $scope.y.contact;
      $scope.userLName = $scope.y.name.substr($scope.y.name.indexOf(' ')+1);
      $scope.userPost = $scope.y.designation;
      $scope.userSkypeId = $scope.y.skypeId;
      $scope.thisPath = $scope.y.userImage;

    };

    $scope.userUpdate = function() {
      $scope.userdetails = false;
      $scope.form = { name : $scope.userFName ,lastName : $scope.userLName, 
                     designation : $scope.userPost,contact:$scope.userContact,
                    skypeId : $scope.userSkypeId ,userImage : $scope.thisPath};
      $http.put('/user/update/'+ $scope.y.email,$scope.form)
      .success( function(data) {
        console.log("updated...",data);
        $cookieStore.remove('userCookie');
        $cookieStore.put('userCookie',data[0]);
        $scope.y= $cookieStore.get('userCookie');
        console.log("new cookie is :" ,$scope.y);
        $scope.named = $scope.y.name.substr(0,$scope.y.name.indexOf(' '));
      }); 
    };
    // $scope.changepassword = false;
    $scope.changePassword = function () {
     // $scope.changepassword= true;
     $scope.userview =false;
     $scope.value = true;
     ngDialog.open ({
        template : '/popups/change.html',
        scope : $scope

     });
    };

    $scope.submitChange = function (changeEmail1,oldPassword1,newPassword1,confirmPass1) {
     console.log(".."+changeEmail1 +".."+oldPassword1+".."+newPassword1+".."+confirmPass1);
      if(!changeEmail1=="" && !oldPassword1=="" && !newPassword1=="" && !confirmPass1=="")
        {  
          if(newPassword1 == confirmPass1) {
            $http.put('user/reset/'+changeEmail1, {pass:newPassword1}).
            success (function(data) {
              console.log("this is new user password:", data);
               
              alert("New Password has been sent to your email-id. \\n Please check your email-id.");
            });
          } else {
             alert("Password doesnot match...");
          }
  
        }  
        else {
          console.log("failure");
          // console.log(".."+changeEmail +".."+oldPassword+".."+newPassword+".."+confirmPass);
        }

    };

    
    $scope.inviteOpen =function(){
      $scope.value = true;
     ngDialog.open ({
        template : '/popups/invite.html',
        scope : $scope

     });
    };

    $scope.inviteNow =function(xyz){
      
      console.log("... in side " ,xyz);
      
        var x = Math.random().toString(36).slice(-8);
        var userVerId = Math.random().toString(36).slice(-8);
        if($scope.addmembers.indexOf(xyz)==-1){

          console.log("invite...........invitin")
        $http.post('/user/register' ,{ projId :$scope.projectnamed,ownerId:$scope.y._id, name :"Your Name", email:xyz , pass:x,verificationID:userVerId, verified: false})
        .success( function(data){
          console.log("updatedddddddddd.....");
          console.log("data is :",data);

        });
      }
      else
      {
       console.log("User is already registered........");
      }
    };

    $scope.allComments = [];

     $scope.addTaskComment = function (){
      console.log($scope.taskcomment);
      $scope.allComments.push({user_Id :$scope.y.email , comment:$scope.taskcomment});
      $http.put('/task/update/'+$scope.hello +'/' + $scope.thisProject._id,{comment: $scope.allComments})
      .success (function (data) {
              console.log("updated....",data);
      });
    };
$scope.projectNames = $scope.projectnamed;
}])
.directive('projectslist', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/projectslist.html'
  };
}).directive('projectdetails', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/projectdetails.html'
  };
}).directive('addprojects', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/addprojects.html'
  };
}).directive('userview', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/userview.html'
  };
}).directive('userdetails', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/userdetails.html'
  };
}).directive('headers', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/headers.html'
  };
}).directive('addmember', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/addmember.html'
  };
}).directive('projectname', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/projectname.html'
  };
}).directive('backlog', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/backlog.html'
  };
}).directive('progress1', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/progress.html'
  };
}).directive('complete', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/complete.html'
  };
}).directive('taskdetails', function() {
  return {
    restrict: 'E',
    templateUrl: 'popups/taskdetails.html'
  };
});



