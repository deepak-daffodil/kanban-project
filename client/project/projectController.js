kanbanApp.controller('projectController', function($scope, $http, $location, $state, $cookieStore, $stateParams, DataService) {
    if ($cookieStore.get('userCookie'))
        $scope.userData = $cookieStore.get("userCookie");
    else
        $state.go("login");

    $scope.$parent.teamview = true;
    $scope.projectnamed = $stateParams.project_name;
    DataService.getWebService($scope, '/project/this/' + $stateParams.project_name + "/" + $scope.userData.email, function(err, data) {
        if (err) {
            console.log("error in tasks get call , ", err);
        } else {
            $scope.thisProject = data[0];
            $scope.$parent.allmembers = $scope.thisProject.members;
            console.log("all members are :", $scope.$parent.allmembers)
            $scope.getTasks();
        }
    });

    $scope.getTasks = function() {
        DataService.getWebService($scope, '/task/list/' + $scope.thisProject.name + "/" + $scope.userData.email, function(err, data) {
            if (err) {
                console.log("error in tasks get call , ", err);
            } else {
                $scope.tasks = data;
                console.log("all tasks are", $scope.tasks)
            }
        });
    };

    $scope.addNewTask = function() {
        $scope.form = {
            projName: $scope.thisProject.name,
            userEmail: $scope.userData.email,
            title: $scope.taskname,
            taskStatus: "backlog",
            seq_no: (($scope.tasks.length) + 1) * 10000
        };
        DataService.postWebService($scope, '/task/add', $scope.form, function(err, data) {
            if (err) {
                console.log("the error in creating the task is :", err);
            } else {
                $scope.getTasks();
                $scope.taskname = "";
                $(".taskText").focus();
            }
        })
    };

    $scope.taskDetails = function(selected_task) {
        $scope.selected_task = selected_task.title;
        $scope.thistaskName = selected_task.title;
        $scope.thistaskStatus = selected_task.taskStatus;
        $scope.thistaskDays = selected_task.timeEstimated;
        $scope.thistaskAssignedTo = selected_task.assignedTo;
        $scope.allComments = selected_task.comment;
        $scope.anything = true;
    };

    $scope.updateTask = function() {
        $scope.form = {
            title: $scope.thistaskName,
            description: $scope.thistaskDesc,
            taskStatus: $scope.thistaskStatus,
            completion_date: $scope.thistaskDate,
            assignedTo: $scope.thistaskAssignedTo,
            timeEstimated: $scope.thistaskDays
        };
        DataService.putWebService($scope, '/task/update/' + $scope.selected_task + "/" + $scope.thisProject.name, $scope.form, function(err, data) {
            if (err) {
                console.log("the error in updating in the task is:", err);
            } else {
                $scope.getTasks();
                $scope.anything = false;
                if ($('[id^=span]').hasClass('glyphicon-ok-sign')) {
                    $('[id^=span]').attr('class', 'paddingRemove glyphicon glyphicon-edit  edit');
                }
            }
        })
    };

    $scope.change = function(id) {
        if ($('#span' + id).hasClass('glyphicon-edit')) {
            $('#span' + id).attr('class', 'paddingRemove glyphicon glyphicon-ok-sign  edit');
            $('#task' + id).removeAttr('readonly');
            $('#task' + id).focus();
        } else {
            $('#task' + id).attr('readonly', 'readonly');
            $('#span' + id).attr('class', 'paddingRemove glyphicon glyphicon-edit  edit')
        }

    };
    $scope.rename_active = function() {
        if ($('#editProject').hasClass('glyphicon-edit')) {
            $('#editProject').attr('class', 'paddingRemove glyphicon glyphicon-ok-sign  edit');
            $('#entername').removeAttr('readonly');
            $('#entername').focus();
            $scope.projectname = $scope.projectnamed;
        } else {
            $('#entername').attr('readonly', 'readonly');
            $('#editProject').attr('class', 'paddingRemove glyphicon glyphicon-edit  edit')
            $scope.renameProject();
        }
    };

    $scope.renameProject = function() {
        console.log("in the rename project : ", $scope.projectnamed);

    };
    var spliceflag = true;
    $scope.dropSuccessHandler = function($event, index, indo, array) {
        if (spliceflag) {
            array.splice(index, 1);
        }
    };

    $scope.onDrop = function($event, index, $data, indo, array) {
        spliceflag = true;
        var thisid = $event.target.id;
        var dodrop = true;
        if (indo == "inprogress" || indo == "completed" || thisid == "progress1" || thisid == "complete1") {
            if (!$data.timeEstimated) {
                alert("Please Give an Estimation Time to move the task..");
                // array.push($data);
                spliceflag = false;
                dodrop = false;

            }
        }
        if (dodrop) {
            console.log("..............", thisid);
            if (thisid == "backlog1") {
                $data.taskStatus = "backlog";
                count = 2;
                // $scope.back1 = false;
            } else if (thisid == "progress1") {
                $data.taskStatus = "inprogress";
                count = 2;
                //$scope.prog1 = false;
            } else if (thisid == "complete1") {
                $data.taskStatus = "completed";
                count = 2;
                // $scope.comp1= false;
            } else {
                $data.taskStatus = indo;
                count = 1;
            }

            if (thisid == "complete1" || indo == "completed") {
                $scope.date = new Date();
                $scope.date1 = new Date($scope.date.getFullYear(), $scope.date.getMonth(), $scope.date.getDate());
                console.log("...................", $scope.date1);
                $data.completion_date = $scope.date1;
                console.log("this is s", $data);
                // $http.post('/completed/' + $scope.userCookie.email, $data)
                //     .success(function(data) {
                //         console.log("sent mail....");
                //     });
            }

            if (count == 1) {
                var newind = -1;
                if ($data.taskStatus == indo) {
                    console.log("coming data is :", $data);
                    for (var j = index - 1; j > 0; j++) {
                        if (array[j].taskStatus == indo) {
                            newind = j;
                            console.log(j);
                            break;
                        }
                    }
                    // if (index==0 || newind == -1) {
                    //   array[index].seq_no = array[index].seq_no /2;
                    //   $data.seq_no = array[index].seq_no / 2 + inc;
                    //   } else {
                    //     $data.seq_no = array[index-1].seq_no + (array[index-1].seq_no/2) + inc;
                    //   }
                    $scope.targetElement = array[index];
                    $data.seq_no = array[index].seq_no - 1;
                    array[index].seq_no = array[index].seq_no + index + 100;
                    // inc +=1;
                }
                // console.log("target data is :", array[index].seq_no);
                count = 0;
            }
            array.push($data);

            $http.put('/task/update/' + $data.title + '/' + $scope.thisProject.name, $data).
            success(function(data) {
                console.log("updated");
                console.log("udtaion is :", data);

                var ind = $scope.tasks.indexOf(data[0]);
                $scope.tasks.splice(ind, 1);
                console.log("task deleted");
                $scope.getTasks();
                $scope.anything = false;
            });
            // }
            if (!count == 2) {
                $http.put('/task/update/' + $scope.targetElement.title + '/' + $scope.thisProject.name, $scope.targetElement).
                success(function(data) {
                    console.log("updated");
                    console.log("udtaion is :", data);

                    var ind = $scope.tasks.indexOf(data[0]);
                    $scope.tasks.splice(ind, 1);
                    console.log("task deleted");
                    $scope.getTasks();
                    $scope.anything = false;
                });
            }
        } // }

    };

}).directive('projectname', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/projectname.html'
    };
}).directive('addmember', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/addmember.html'
    };
}).directive('backlog', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/backlog.html'
    };
}).directive('progress1', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/progress.html'
    };
}).directive('complete', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/complete.html'
    };
}).directive('taskdetails', function() {
    return {
        restrict: 'E',
        templateUrl: 'popups/taskdetails.html'
    };
});
