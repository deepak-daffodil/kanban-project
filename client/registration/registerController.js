kanbanApp.controller('registerController',function($scope,$http, $location,$state, DataService) {
  $scope.confirmP = false;
  $scope.add  =  function () {
    $scope.form  =  {
                      name:$scope.user, 
                      email :$scope.mail,
                      pass: $scope.pass ,
                      verified:false,
                      verificationID : Math.random().toString(36).slice(-8)
                    };
    if($scope.pass == $scope.confirmpassword) {
      $scope.confirmP = false;
      DataService.postWebService( $scope,'/user/register', $scope.form,function(err ,data) {
        if(err) {
          console.log("error is ",err);
        } else {
           $state.go('message');
        }
      })
      
    }
  };
});
