kanbanApp.controller('loginController', function($scope, $http, $location, $state, $cookieStore, DataService) {

    $scope.login = function() {
        console.log("enter in post call");
        $scope.form = {
            email: $scope.user,
            pass: $scope.pass
        };


        $http.get('/user/details/'+$scope.email, $scope.form)
            .success(function(result) {
                var cookie = {};
                cookie["name"] = result.name;
                cookie["email"] = result.email;

                $cookieStore.put('userCookie', cookie);

                console.log("cookie is ", $cookieStore.get('userCookie'));
                $state.go('dash');
        })



        DataService.postWebService($scope, "/user/login", $scope.form, function(err, result) {
            if (err) {
                console.log("error is ");
            } else {
                console.log("no error ", result);
                if (result.length != 0) {
                    console.log("............", result.verified);
                    if (result.verified == true) {
                        var cookie = {};
                        cookie["name"] = result.name;
                        cookie["email"] = result.email;

                        $cookieStore.put('userCookie', cookie);

                        console.log("cookie is ", $cookieStore.get('userCookie'));
                        $state.go('dash');
                    } else {
                        alert("It seems you have not verify your Email-Id yet.\n Please Verify Your Email-Id to access your account");
                    }
                } else {
                    $scope.wrong = true;
                }
            }
        })
    };
});
