var kanbanApp  =  angular.module('kanbanApp', ['ui.router','ngCookies' , 'ang-drag-drop','ngAnimate','angularFileUpload','ngDialog']);


kanbanApp.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  // HOME STATES AND NESTED VIEWS  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  = 
  .state('login', {
    url: '/login',
    templateUrl: '/login/login.html',
    controller: 'loginController',
  })
  .state('reset', {
    url: '/reset-password',
    templateUrl: '/login/reset.html',
    controller: 'resetController',     
  })      
  .state('change', {
    url: '/user/change',
    templateUrl: '/registration/change.html',
    controller: 'changepasswordController',
  })
  .state('message', {
    url: '/register/success',
    templateUrl: '/registration/message.html',
    controller: 'successController'
    
  })
  .state('verify', {
    url: '/verify/:ver_id/email/:email_id',
    templateUrl: '/login/verify.html',
    controller: 'verifyController',     
  })
  .state('register', {
    url: '/register',
    templateUrl: '/registration/reg.html',
    controller: 'registerController',
  })
  .state('dash', {
    url: '/dashboard',
    templateUrl: '/dashboard/dash.html',
    controller: 'dashboardController',       
  })
  .state('dash.project', {
    url: '/:project_name',
    templateUrl: '/project/project.html',
    controller: 'projectController',       
  })
  .state('userprofile', {
    url: '/profile/:user_email',
    templateUrl: '/profile/user_detail.html',
    controller: 'profileController',       
  })
  .state('change_password', {
    url: '/change_password/',
    templateUrl: '/change_password/change.html',
    controller: 'changepasswordController',       
  })


  $urlRouterProvider.otherwise('/login');
});
